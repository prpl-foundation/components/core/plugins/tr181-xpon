MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include_priv)

SOURCES = $(wildcard $(SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 \
          $(addprefix -I ,$(INCDIR)) \
          -fkeep-inline-functions -fkeep-static-functions \
          -Wno-format-nonliteral -Wno-unused-variable \
          -Wno-unused-but-set-variable \
          $(shell pkg-config --cflags cmocka) \
          -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL_DEFAULT=500

# Many plugins use following flag. I put it in comment because I do not always
# provide a declaration. E.g. there's no declaration for _onu_enable_changed().
# IMO that's not a problem as it's not needed.
#CFLAGS += -Wmissing-declarations

CFLAGS += -DMAX_NR_OF_ONUS=1

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
           $(shell pkg-config --libs cmocka) \
           -lamxo -lamxb -lamxd -lamxp -lamxm -lamxc \
           -lsahtrace

# Link to dl because of dlerror() call
LDFLAGS += -ldl


