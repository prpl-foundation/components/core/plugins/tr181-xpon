/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * @file persistency.c
 *
 * Functionality related to saving and querying the persistent settings. The
 * only read-write parameters in the BBF XPON DM which need to be persistent
 * are:
 * - XPON.ONU.{i}.Enable
 * - XPON.ONU.{i}.ANI.{i}.Enable
 * - XPON.ONU.{i}.ANI.{i}.TC.Authentication.HexadecimalPassword
 * - XPON.ONU.{i}.ANI.{i}.TC.Authentication.Password
 *
 * The object instances do not exist right after the plugin has loaded the odl
 * files defining the DM, and the _defaults.odl file. The plugin creates them
 * afterwards, while querying the state in the HAL, or when it receives a
 * notification from the HAL to create such an instance. Therefore the plugin
 * does not use the odl mechanism to save and restore the values of the
 * parameters above.
 *
 * If one of the Enable parameters is set to true, the persistency part creates
 * an empty file in the storage dir of this plugin. E.g., if XPON.ONU.1.Enable
 * is set to true, it creates XPON.ONU.1_enabled.txt. If the plugin after reboot
 * creates the instance XPON.ONU.1, it checks if that file exists to determine
 * if it should set XPON.ONU.1.Enable to true.
 */


/**
 * Define _GNU_SOURCE to avoid following error:
 * implicit declaration of function ‘symlink’
 */
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

/* Related header */
#include "persistency.h"

/* System headers */
#include <errno.h>
#include <stdio.h>            /* fopen() */
#include <stdlib.h>           /* free() */
#include <string.h>
#include <unistd.h>           /* access() */

/* Other libraries' headers */
#include <amxc/amxc_macros.h> /* when_null() */
#include <amxp/amxp_dir.h>    /* amxp_dir_is_directory() */

/* Own headers */
#include "xpon_mngr.h"          /* xpon_mngr_get_parser() */
#include "file_utils.h"         /* write_file() */
#include "password_constants.h" /* MAX_PASSWORD_LEN_PLUS_ONE */
#include "xpon_trace.h"

static const char* const SYMLINK_TO_STORAGE_DIR = "/etc/config/tr181-xpon";

/* Path to storage dir of this plugin, e.g., "/cfg/pcm/tr181-xpon/". */
static char* s_storage_dir = NULL;

/**
 * Return the storage dir.
 *
 * The returned pointer must be freed.
 *
 * @return the storage dir upon success, else NULL
 */
static char* get_storage_dir(void) {

    char* result = NULL;
    amxo_parser_t* parser = xpon_mngr_get_parser();
    when_null(parser, exit);

    amxc_string_t dir;
    amxc_string_init(&dir, 0);
    const char* const storage_dir = GETP_CHAR(&parser->config, "storage-path");
    if(!storage_dir) {
        SAH_TRACEZ_ERROR(ME, "No storage-path");
        amxc_string_clean(&dir);
        goto exit;
    }
    /* storage_dir looks like '${rw_data_path}/${name}/'. Resolve it. */
    amxc_string_setf(&dir, "%s", storage_dir);
    amxc_string_resolve(&dir, &parser->config);
    result = amxc_string_take_buffer(&dir);

exit:
    return result;
}

/**
 * Create symlink /etc/config/tr181-xpon to /cfg/pcm/tr181-xpon.
 *
 * tr181-xpon does not use /etc/config/tr181-xpon. It only creates the symlink
 * for user convenience. Users might look in /etc/config for debugging because
 * that's where most components store their settings.
 */
static void create_symlink_to_storage_dir(void) {
    if(access(SYMLINK_TO_STORAGE_DIR, F_OK) == 0) {
        /* SYMLINK_TO_STORAGE_DIR already exists */
        return;
    }
    when_null(s_storage_dir, exit);
    if(symlink(s_storage_dir, SYMLINK_TO_STORAGE_DIR) == -1) {
        SAH_TRACEZ_WARNING(ME, "Failed to create %s: %s",
                           SYMLINK_TO_STORAGE_DIR, strerror(errno));
    }
exit:
    return;
}

/**
 * Initialize the persistency part.
 *
 * Ask the parser for the storage dir. Set 's_storage_dir' to that value if the
 * storage dir ends with '/' and exists. The Ambiorix framework in tr181-xpon
 * should normally should have created the folder when tr181-xpon is started
 * (and if the folder did not exist yet).
 *
 * Call create_symlink_to_storage_dir() if s_storage_dir != NULL.
 *
 * The plugin must call this function once at startup.
 */
void persistency_init(void) {

    char* storage_dir = get_storage_dir();
    when_null(storage_dir, exit);

    const size_t storage_dir_len = strlen(storage_dir);
    when_true_trace(0 == storage_dir_len, exit, ERROR, "storage dir has zero length");

    const char last_char = storage_dir[storage_dir_len - 1];
    when_true_trace(last_char != '/', exit, ERROR,
                    "storage_dir='%s': last char is not /", storage_dir);

    if(!amxp_dir_is_directory(storage_dir)) {
        SAH_TRACEZ_ERROR(ME, "%s does not exist", storage_dir);
        goto exit;
    }

    s_storage_dir = storage_dir;

    create_symlink_to_storage_dir();

exit:
    if(!s_storage_dir) {
        SAH_TRACEZ_ERROR(ME, "Failed to have a folder for persistency");
        /* Only free 'storage_dir' if it was not assigned to s_storage_dir */
        free(storage_dir);
    }
}

/**
 * Clean up the persistency part.
 *
 * The plugin must call this function once when stopping.
 */
void persistency_cleanup(void) {
    if(s_storage_dir) {
        free(s_storage_dir);
        s_storage_dir = NULL;
    }
}

/**
 * Touch file if it does not exist yet.
 *
 * @param[in] file: file path
 */
static void touch_file(const char* const file) {
    /* Avoid writing the file if it already exists */
    if(access(file, F_OK) == 0) {
        SAH_TRACEZ_DEBUG2(ME, "%s already exists", file);
        return;
    }

    FILE* f = fopen(file, "w");
    if(NULL == f) {
        SAH_TRACEZ_ERROR(ME, "Failed to create '%s': %s", file, strerror(errno));
    } else {
        fclose(f);
    }
}

static void create_enabled_file_path(amxc_string_t* const file_path,
                                     const char* const object) {

    amxc_string_set(file_path, s_storage_dir);
    amxc_string_appendf(file_path, "%s", object);
    amxc_string_append(file_path, "_enabled.txt", 12);
}

/**
 * Save whether object is enabled or disabled.
 *
 * @param[in] object  path to object in DM being enabled/disabled, e.g., "XPON.ONU.1"
 * @param[in] enable  true if object is enabled, else false
 */
void persistency_enable(const char* const object, bool enable) {

    when_null_trace(s_storage_dir, exit, DEBUG, "No persistency");
    when_null(object, exit);

    amxc_string_t file_path;
    amxc_string_init(&file_path, 0);
    create_enabled_file_path(&file_path, object);

    const char* const file_path_cstr = amxc_string_get(&file_path, 0);

    SAH_TRACEZ_DEBUG(ME, "object='%s enable=%d", object, enable);

    if(enable) {
        touch_file(file_path_cstr);
    } else {
        unlink(file_path_cstr);
    }

    amxc_string_clean(&file_path);

exit:
    return;
}

/**
 * Return true if object identified by 'object' is enabled.
 *
 * @param[in] object path to object in DM being queried, e.g. "XPON.ONU.1".
 *
 * @return true if object is enabled, else false.
 */
bool persistency_is_enabled(const char* const object) {

    bool rv = false;
    when_null_trace(s_storage_dir, exit, DEBUG, "No persistency");
    when_null(object, exit);

    amxc_string_t file_path;
    amxc_string_init(&file_path, 0);
    create_enabled_file_path(&file_path, object);

    const char* const file_path_cstr = amxc_string_get(&file_path, 0);

    rv = (access(file_path_cstr, F_OK) == 0) ? true : false;

    amxc_string_clean(&file_path);

exit:
    return rv;
}


static void create_password_file_path(const char* const ani_object,
                                      bool hex,
                                      amxc_string_t* const file_path) {

    amxc_string_set(file_path, s_storage_dir);
    amxc_string_appendf(file_path, "%s", ani_object);
    if(hex) {
        amxc_string_append(file_path, "_password_hex.txt", 17);
    } else {
        amxc_string_append(file_path, "_password_ascii.txt", 19);
    }
}

/**
 * Save the password of a certain ANI instance.
 *
 * @param[in] ani_path: path to ANI instance, e.g. "XPON.ONU.1.ANI.1"
 * @param[in] password: new value for the parameter Password of the ANI
 *                      referred to by @a ani_path
 * @param[in] hex: if true @a password is in hexadecimal format; if false,
 *                 @a password is in ASCII format. The function only uses
 *                 this parameter if @a password is not empty.
 *
 * If @a password is empty, the function removes any files storing the password
 * for the ANI instance.
 */
void persistency_set_password(const char* const ani_path,
                              const char* const password,
                              bool hex) {

    when_null_trace(s_storage_dir, exit, DEBUG, "No persistency");
    when_null(ani_path, exit);
    when_null(password, exit);

    SAH_TRACEZ_DEBUG(ME, "ani_path='%s' password='%s' hex=%d",
                     ani_path, password, hex);

    amxc_string_t file_path_ascii;
    amxc_string_t file_path_hex;
    amxc_string_init(&file_path_ascii, 0);
    amxc_string_init(&file_path_hex, 0);

    create_password_file_path(ani_path, /*hex=*/ false, &file_path_ascii);
    create_password_file_path(ani_path, /*hex=*/ true, &file_path_hex);

    const char* const ascii_cstr = amxc_string_get(&file_path_ascii, 0);
    const char* const hex_cstr = amxc_string_get(&file_path_hex, 0);

    if(strlen(password) == 0) {
        unlink(ascii_cstr);
        unlink(hex_cstr);
    } else {
        write_file(hex ? hex_cstr : ascii_cstr, password);
        unlink(hex ? ascii_cstr : hex_cstr);
    }

    amxc_string_clean(&file_path_ascii);
    amxc_string_clean(&file_path_hex);

exit:
    return;
}

#define ASCII_PASSWORD_FILE_INDEX 0
#define HEX_PASSWORD_FILE_INDEX 1

/**
 * Get the saved password for a certain ANI instance.
 *
 * @param[in] ani_path: path to ANI instance, e.g. "XPON.ONU.1.ANI.1"
 * @param[in,out] password: the function writes the saved password to this
 *     parameter if a saved password for the ANI exists. If no saved password
 *     exists, the function does not write to this parameter. The caller must
 *     pass a buffer of size MAX_PASSWORD_LEN_PLUS_ONE to ensure this function
 *     has enough space to write a password.
 * @param[in,out] hex: the function sets this parameter to true if the password
 *     it writes to @a password is in hexadecimal format.
 *
 * If no saved password exists for the ANI, the function does not update
 * @a password or @a hex, and returns true.
 *
 * @return true on success, else false
 */
bool persistency_get_password(const char* const ani_path,
                              char* const password,
                              bool* hex) {

    bool rv = false;
    int i;
    char password_saved[MAX_PASSWORD_LEN_PLUS_ONE] = { 0 };
    const char* password_file_used = NULL;

    amxc_string_t file_path_ascii;
    amxc_string_t file_path_hex;
    amxc_string_init(&file_path_ascii, 0);
    amxc_string_init(&file_path_hex, 0);

    create_password_file_path(ani_path, /*hex=*/ false, &file_path_ascii);
    create_password_file_path(ani_path, /*hex=*/ true, &file_path_hex);
    const char* files[2] = { 0 };
    files[ASCII_PASSWORD_FILE_INDEX] = amxc_string_get(&file_path_ascii, 0);
    files[HEX_PASSWORD_FILE_INDEX] = amxc_string_get(&file_path_hex, 0);

    for(i = 0; i < 2; ++i) {
        if(access(files[i], F_OK) == 0) {
            if(!read_first_line_from_file(files[i], password_saved,
                                          MAX_PASSWORD_LEN_PLUS_ONE)) {
                SAH_TRACEZ_ERROR(ME, "Failed get password from %s", files[i]);
                goto exit;
            }
            password_file_used = files[i];
            if(HEX_PASSWORD_FILE_INDEX == i) {
                *hex = true;
            }
            break;
        }
    }
    if(password_file_used == NULL) {
        SAH_TRACEZ_DEBUG(ME, "%s: no password", ani_path);
        rv = true;
        goto exit;
    }


    if(strlen(password_saved) == 0) {
        SAH_TRACEZ_ERROR(ME, "Password read from %s is empty",
                         password_file_used);
        goto exit;
    }

    if(snprintf(password, MAX_PASSWORD_LEN_PLUS_ONE, "%s",
                password_saved) < 0) {
        SAH_TRACEZ_ERROR(ME, "snprintf() to copy password failed");
        goto exit;
    }
    SAH_TRACEZ_DEBUG(ME, "ani='%s': password='%s' hex=%d", ani_path,
                     password, *hex);
    rv = true;

exit:
    amxc_string_clean(&file_path_ascii);
    amxc_string_clean(&file_path_hex);
    return rv;
}


