#!/bin/sh

# shellcheck disable=SC1091 # Do not follow sourced files
. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-xpon"
datamodel_root="XPON"

# tr181-xpon no longer uses /etc/config/tr181-xpon for persistency. It uses 
# /cfg/pcm/tr181-xpon. Remove the folder /etc/config/tr181-xpon if it exists.
persistency_migration_cleanup(){
    if [ -d /etc/config/tr181-xpon ]; then
        rm -rf /etc/config/tr181-xpon
    fi
}

case $1 in
    boot)
        persistency_migration_cleanup
        process_boot ${name} -D
        ;;
    start)
        persistency_migration_cleanup
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo|d)
        process_debug_info ${datamodel_root}
        if [ -x /usr/lib/amx/tr181-xpon/modules/debuginfo_vendor ]; then
            /usr/lib/amx/tr181-xpon/modules/debuginfo_vendor
        fi
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
