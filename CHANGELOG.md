# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.4.8 - 2024-12-24(15:01:21 +0000)

### Other

- tr181-xpon: add first unit test

## Release v1.4.7 - 2024-10-11(15:57:08 +0000)

### Fixes

- tr181-xpon: persistence is broken

## Release v1.4.6 - 2024-10-10(12:07:36 +0000)

### Fixes

- tr181-xpon no longer dumps vendor specific info

## Release v1.4.5 - 2024-07-29(14:03:57 +0000)

### Other

- Add tr181-device proxy odl files to various components

## Release v1.4.4 - 2024-07-25(14:04:51 +0000)

### Fixes

- tr181-xpon: general protection fault in libc.so while stopping the service

## Release v1.4.3 - 2024-07-23(07:59:02 +0000)

### Fixes

- Better shutdown script

## Release v1.4.2 - 2024-04-26(13:24:44 +0000)

## Release v1.4.1 - 2024-04-15(08:48:33 +0000)

### Fixes

- - tr181-xpon: save settings in upgrade persistent folder

## Release v1.4.0 - 2023-11-29(14:02:20 +0000)

### New

- [CHR2] The Device.XPON. object should be removed form the Device mapper.

## Release v1.3.8 - 2023-11-27(15:45:15 +0000)

### Fixes

- tr181-xpon: adapt to prpl community builds

## Release v1.3.7 - 2023-10-06(12:04:47 +0000)

### Changes

- tr1810-xpon: call debuginfo script of vendor module if present

## Release v1.3.6 - 2023-10-04(16:59:06 +0000)

### Fixes

- tr181-xpon: crash upon receiving GEM ports

## Release v1.3.5 - 2023-10-02(12:20:24 +0000)

### New

- Support PON password

### Fixes

- Fix debug message about nonexistent ONU instance

## Release v1.3.4 - 2023-09-20(15:45:54 +0000)

### New

- - PON vendor module: show correct IF to non-OMCI domain

## Release v1.3.3 - 2023-09-18(15:18:22 +0000)

### New

- [PON] Support entire XPON DM

## Release v1.3.2 - 2023-08-08(10:38:09 +0000)

### Fixes

- - tr181-xpon: improve enabling an ONU at startup
- - tr181-xpon: persistency broken

### Changes

- - tr181-xpon: set import-dbg to false

## Release v1.3.1 - 2023-08-03(13:38:17 +0000)

### Fixes

- [PON] make ModuleName and ModuleError protected

## Release v1.3.0 - 2023-07-31(14:47:05 +0000)

### New

- [tr181-xpon] Add support for defaults.d directory

## Release v1.2.2 - 2023-07-26(12:34:29 +0000)

### New

- - [TR-181][PON] Support LastChange parameters

## Release v1.2.1 - 2023-07-25(09:02:40 +0000)

### Fixes

- [PON] Support selection VEIP vs PPTP Ethernet UNI

## Release v1.2.0 - 2023-07-17(10:09:54 +0000)

### New

- [PON] Support selection VEIP vs PPTP Ethernet UNI

## Release v1.1.5 - 2023-07-12(10:16:56 +0000)

### New

- [PON] Support BCM WAN mode configuration

## Release v1.1.4 - 2023-07-04(09:36:17 +0000)

### New

- [PON] Support PON SN

## Release v1.1.3 - 2023-06-02(09:55:01 +0000)

### Other

- - XPON manager: update README.md

## Release v1.1.2 - 2023-05-11(12:46:32 +0000)

### Other

- tr181-xpon: improve loading of vendor module

## Release v1.1.1 - 2023-05-08(08:54:39 +0000)

### Other

- tr181-xpon: add features and improve implementation

## Release v1.1.0 - 2023-05-04(09:40:03 +0000)

### New

- Sync xpon EthernetUNI with NetModel

## Release v1.0.5 - 2023-02-03(18:54:41 +0000)

### Other

- - [PRPL][PON] Handle open issues including BBF changes

## Release v1.0.4 - 2022-10-21(15:02:27 +0000)

### Other

- - XPON manager: ModuleVersion of Transceiver is missing

## Release v1.0.3 - 2022-08-30(11:58:02 +0000)

### Fixes

- Correct import of the mod-xpon-prpl module

## Release v1.0.2 - 2022-08-25(13:46:44 +0000)

### Other

- Opensource component

## Release v1.0.1 - 2022-08-23(13:37:25 +0000)

### New

- - Create PON manager
- ci: add gitlab-ci.yml

## Release v1.0.0 - 2022-08-22(14:34:37 +0000)

### New

- - Create PON manager

### Other

- - Create PON manager

